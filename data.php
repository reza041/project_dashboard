<?php

  $curl = curl_init();
  curl_setopt($curl, CURLOPT_URL, 'https://api.covid19api.com/summary');
  curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
  $result = curl_exec($curl);
  curl_close($curl);

  $result = json_decode($result, true);

  $contries = $result['Countries'];
  foreach ($contries as $key=>$value){
     //date
  }

?>

  <!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <link rel="shortcut icon" href="images/covid-19.png" type="image/x-icon" class="rounded-circle">

    <title>Data Covid-19</title>
  </head>
  <body>

  <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
  <img class="ml-auto" src="images/logo who.png" width="50px" height="50px" alt="">
  <div class="container">
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
      <div class="collapse navbar-collapse" id="navbarNav">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item text-uppercase">
            <a class="nav-link" href="index.php">Dashboard<span class="sr-only">(current)</span></a>
          </li>
          <li class="nav-item text-uppercase">
            <a class="nav-link" href="index.php">Data</a>
          </li>
          <li class="nav-item text-uppercase">
           <a class="nav-link" href="index.php">Grafik</a>
          </li>
          <li class="nav-item text-uppercase">
            <a class="nav-link disabled" href="data.php" >Data Table</a>
          </li>
        </ul>
      </div>
    </div>
  </nav>

  <div class="container mt-5">
    <div class="container pt-5">
      <center><h4><b> SEBARAN KASUS <i style="color: red;">TERKONFIRMASI</i> PER NEGARA </b></h4></center>
      <p class="text-center"><b>Pembaruan Terakhir</b> : <?php echo $value['Date'];?><br><b>Sumber</b> : <a style="text-decoration: none;" class="text-muted" href="https://api.covid19api.com/summary">covid19.com</a></p>
    </div>
  </div>
    
  <div class="container mt-5">
    <div class="table-responsive shadow px-2">
      <table class="table table-hovered table-bordered table-sm text-center">
        <thead>
          <tr>
            <th rowspan="3" class="text-left align-middle ">Negara</th>
            <th colspan="6" class="text-danger">Terkonfirmasi</th>
          </tr>
          <tr>
            <th rowspan="2" class="text-info text-right align-middle">Total Positif</th>
            <th colspan="3" class="text-danger">Pertambahan</th>
            <th rowspan="2" class="text-success text-right align-middle">Total Sembuh</th>
            <th rowspan="2" class="text-right align-middle">Total Meninggal</th>
          </tr>
          <tr>
            <th scope="col" class="text-danger">Sembuh</th>
            <th scope="col" class="text-danger">Positif</th>
            <th scope="col" class="text-danger">Meninggal</th>
          </tr>
        </thead>
        <?php foreach($contries as $key): ?>
        <tbody>
        <tr>
            <td class="text-left align-middle"><?php echo $key['Country'] ?></td>
            <td class="text-right align-middle"><?php echo number_format($key['TotalConfirmed']) ?></td>
            <td class="text-right align-middle"><?php echo number_format($key['NewRecovered']) ?></td>
            <td class="text-right align-middle"><?php echo number_format($key['NewConfirmed']) ?></td>
            <td class="text-right align-middle"><?php echo number_format($key['NewDeaths']) ?></td>
            <td class="text-right align-middle"><?php echo number_format($key['TotalRecovered']) ?></td>
            <td class="text-right align-middle"><?php echo number_format($key['TotalDeaths']) ?></td>
          </tr>
          
        </tbody>
        <?php endforeach; ?>
      </table>
    </div>
  </div>


  

 


    <!-- Optional JavaScript; choose one of the two! -->

    <!-- Option 1: jQuery and Bootstrap Bundle (includes Popper) -->
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>

    <!-- Option 2: jQuery, Popper.js, and Bootstrap JS
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.min.js" integrity="sha384-w1Q4orYjBQndcko6MimVbzY0tgp4pWB4lZ7lr30WKz0vr/aWKhXdBNmNb5D92v7s" crossorigin="anonymous"></script>
    -->
  </body>
</html>