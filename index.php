<?php

  $curl = curl_init();
  curl_setopt($curl, CURLOPT_URL, 'https://api.covid19api.com/summary');
  curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
  $result = curl_exec($curl);
  curl_close($curl);

  $result = json_decode($result, true);

  $global = $result['Global'];
  $temp_all = $global;
  $temp_NewConfirmed = (string)$global['NewConfirmed'];
  $temp_TotalConfirmed = (string) $global['TotalConfirmed'];
  $temp_NewDeaths = (string) $global['NewDeaths'];
  $temp_TotalDeaths = (string) $global['TotalDeaths'];
  $temp_NewRecovered = (string) $global['NewRecovered'];
  $temp_TotalRecovered = (string) $global['TotalRecovered'];
  $temp_persent1= $temp_all["TotalRecovered"]/$temp_all['TotalConfirmed']*100;
  $temp_persent2= $temp_all["TotalDeaths"]/$temp_all['TotalConfirmed']*100;
  $temp_persent3= ($temp_all['TotalConfirmed']-($temp_all["TotalRecovered"]+$temp_all["TotalDeaths"]))/$temp_all['TotalConfirmed']*100;

  $contries = $result['Countries'];
  foreach ($contries as $key=>$value){
     //date
  }


?>

<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.15.1/css/all.css" integrity="sha384-vp86vTRFVJgpjF9jiIGPEEqYqlDwgyBgEF109VFjmqGmIY/Y4HV4d3Gp2irVfcrp" crossorigin="anonymous"> 
    <link rel="shortcut icon" href="images/covid-19.png" type="image/x-icon" class="rounded-circle">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.4/Chart.min.js"
            integrity="sha512-d9xgZrVZpmmQlfonhQUvTR7lMPtO7NkZMkA0ABN3PHCbKA5nqylQ/yWlFAyY6hYgdF1Qh6nYiuADWwKB4C2WSw=="
            crossorigin="anonymous"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

    <title>Data Covid-19</title>
  </head>
  <body>

  <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
  <img class="ml-auto" src="images/logo who.png" width="50px" height="50px" alt="">
  <div class="container">
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNav">
      <ul class="navbar-nav ml-auto">
        <li class="nav-item text-uppercase">
          <a class="nav-link" href="#">Dashboard<span class="sr-only">(current)</span></a>
        </li>
        <li class="nav-item text-uppercase">
          <a class="nav-link" href="#data">Data</a>
        </li>
        <li class="nav-item text-uppercase">
          <a class="nav-link" href="#grafik">Grafik</a>
        </li>
        <li class="nav-item text-uppercase">
          <a class="nav-link" href="data.php">Data tabel</a>
        </li>
      </ul>
    </div>
  </div>
</nav>



  <div class="container bg-danger mt-5">
    <center><img class="mt-5" src="images/logo who.png" alt="" width="200px" height="200px"></center>
    <div class="container bg-light p-5">
        <h4 class="text-center">Satuan Tugas Penangan</h4>
        <h2 class="text-center">COVID-19</h2>
        <h2 class="text-center">INDONESIA</h2>
        <center><hr width="5%" style="background-color: green;"></center>
    </div>
    <div class="container p-5">
      <div class="row">
          <div class="col-lg-6 col-sm-12 p-3 bg-light">
            <div class="card shadow">
              <h4 class="text-center">HOLINE TANGGAP COVID-19 <br> INDONESIA </h4>
              <a class="p-3" style="text-decoration: none;" href="tel:0811-2716-119"><button class="btn btn-danger btn-block border-0 mb-1 hotline rounded-8"><i style="font-size: 2rem;" class="fab fa-whatsapp"></i> 0811-2716-119 <br> layanan kegawatdaruratan dan info COVID-19</button></a>
              <a class="p-3" style="text-decoration: none;" href="https://api.whatsapp.com/send?phone=0811-2716-119"><button class="btn btn-success btn-block border-0 mb-1 hotline rounded-8"><i style="font-size: 2rem;" class="fab fa-whatsapp"></i> 0811-2716-119 <br> layanan kegawatdaruratan dan info COVID-19</button></a>
            </div>
          </div>

        <div class="col-lg-6 col-sm-12 p-3 bg-light">
          <div class="card shadow">
            <div class="card-body">
              <h4>CEK KONDISI ANDA !</h4>
              <p class="text-muted">Deteksi Mandiri Cepat COVID-19 adalah salah satu cara untuk mengetahi apakah Anda memiliki gejala yang memerlukan pemeriksaan dan pengujian lebih lanjut mengenai COVID-19 atau tidak.</p>
              <a style="text-decoration: none;" href="https://corona.jatengprov.go.id/screening"><button class="btn btn-danger btn-block border-0 mb-1 hotline rounded-8">Cek Disini</button></a>
            </div>
          </div>
        </div>
    </div>
  </div>
  </div>

  <div id="data" class="site-section sectien-counter">
    <div class="container pt-5">
      <div class="container mt-4">
        <h3 class="text-center">Data Kasus Covid-19 di Dunia</h3>
        <p class="text-center"><b>Pembaruan Terakhir</b> : <?php echo $value['Date'];?><br><b>Sumber</b> : <a style="text-decoration: none;" class="text-muted" href="https://api.covid19api.com/summary">covid19.com</a></p>
      </div>
      <div class="container mt-5 mb-4">
        <div class="card-deck">
          <div class="card">
            <div class="card-footer" style="background-color: 	#FFEBCD;">
              <small>
                  <center><h5 class="fa fa-circle">Terkonfirmasi Covid-19</h5></center>
              </small>
            </div>

            <div class="card-body">
              <center><h3 class="card-text" style="color: red;"><?php echo number_format($temp_TotalConfirmed); ?></h3></center>
              <hr>
              <div class="container">
                <div class="row">
                  <div class="col-12 ">
                    <p class="text-danger"><b>Pertambahan Pasien <br> <?php echo number_format($temp_NewConfirmed);?></b></p>
                  </div>
                </div>
              </div>
            </div>
          </div>
    
          <div class="card">
            <div class="card-footer" style="background-color: #F0F8FF;">
              <small class="text-center">
                  <center><h5 class="fa fa-circle">Pasien Sembuh</h5></center>
              </small>
            </div>
            <div class="card-body">
              <center><h3 class="card-title" style="color: yellow;"><?php echo number_format($temp_TotalRecovered); ?></h3></center>
              <hr>
              <div class="container">
                <div class="row">
                  <div class="col-12">
                    <p class="text-success"><b>Pertambahan Sembuh <br> <?php echo number_format($temp_NewRecovered);?></b></p>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="card">
            <div class="card-footer" style="background-color: #7FFFD4;">
              <small class="text-center">
                  <center><h5 class="fa fa-circle">Pasien Meninggal</h5></center>
              </small>
            </div>
            <div class="card-body">
              <center><h3 class="card-title" style="color: green;"><?php echo number_format($temp_TotalDeaths); ?></h3></center>
              <hr>
              <div class="container">
                <div class="row">
                  <div class="col-12">
                    <p><b>Pertambahan Meninggal <br> <?php echo number_format($temp_NewDeaths);?></b></p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>


  <div id="grafik" class="site-section sectien-counter">
    <br>

    <div class="container pt-5">
      <center><h4><b>  GRAFIK <i style="color: red;">COVID-19</i> PER NEGARA </b></h4></center>
      <p class="text-center"><b>Pembaruan Terakhir</b> : <?php echo $value['Date'];?><br><b>Sumber</b> : <a style="text-decoration: none;" class="text-muted" href="https://api.covid19api.com/summary">covid19.com</a></p>
    </div>
    <div id="carouselExampleIndicators" class="carousel slide p-4" data-ride="carousel">
  <ol class="carousel-indicators">
    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
  </ol>
  <div class="carousel-inner">
    <div class="carousel-item active ">
      <div class="container pt-4">
        <div class="card shadow">
          <h4 class="ml-4 p-3">Grafik Terkonfirmasi COVID-19</h4>
          <div class="col-lg-12 col-sm-12">
            <div class="col-lg-8 col-sm-12 float-left">
              <canvas id="myThridChart"></canvas>
            </div>
              <div class="col-lg-4 col-sm-6 float-left">
                <div class="card">
                  <div class="card-footer" style="background-color: #FFEBCD;">
                    <small class="text-center">
                      <center><h4>Pasien yang Dirawat</h4></center>
                    </small>
                  </div>
                  <div class="card-body p-3">
                    <center><h3 class="card-title"> <?php echo substr($temp_persent3, 0, 5)."%" ; ?></h3></center>
                  </div>
                </div>
              </div>
          </div>
        </div>
      </div>
    </div>
    <div class="carousel-item">
      <div class="container pt-4">
        <div class="card shadow">
          <h4 class="ml-4 p-3">Grafik Pasien Sembuh</h4>
          <div class="col-lg-12 col-sm-12">
            <div class="col-lg-8 col-sm-12 float-left">
              <canvas id="myChart"></canvas>
            </div>
            <div class="col-lg-4 col-sm-6 float-left">
              <div class="card">
                <div class="card-footer" style="background-color: #F0F8FF;">
                  <small class="text-center">
                    <center><h4>Presentasi Sembuh</h4></center>
                  </small>
                </div>
                <div class="card-body p-3">
                  <center><h3 class="card-title"> <?php echo substr($temp_persent1, 0, 5)."%" ; ?></h3></center>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="carousel-item">
          <div class="container pt-4">
      <div class="card shadow">
        <h4 class="ml-4 p-3">Grafik Pasien Meninggal</h4>
        <div class="col-lg-12 col-sm-12">
          <div class="col-lg-8 col-sm-12 float-left">
              <canvas id="mySecondChart"></canvas>
          </div>
          <div class="col-lg-4 col-sm-6 float-left">
            <div class="card">
              <div class="card-footer" style="background-color: #7FFFD4;">
                <small class="text-center">
                  <center><h4>Presentasi Kematian</h4></center>
                </small>
              </div>
              <div class="card-body p-3">
                <center><h3 class="card-title"><?php echo substr($temp_persent2, 0, 4)."%"; ?></h3></center>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    </div>
  </div>
  <a class=" carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
    <span class=" bg-dark carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
    <span class="bg-dark carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>

    <script>
        var ctx = document.getElementById('myChart').getContext('2d')
        var cty = document.getElementById('mySecondChart').getContext('2d');
        var ctz = document.getElementById('myThridChart').getContext('2d')

        var covid = $.ajax({
            url: "https://api.covid19api.com/summary",
            cache : false
        })

        .done(function (canvas) {
            
            
            function getContries(canvas) {
                var show_country=[];

                canvas.Countries.forEach(function(el) {
                    show_country.push(el.Country);
                })
                return show_country;
            }


            function getHealth(canvas) {
                var recovered=[];

                canvas.Countries.forEach(function(el) {
                    recovered.push(el.TotalRecovered)
                })
                return recovered;
            }
            function getDeath(canvas) {
                var deaths=[];

                canvas.Countries.forEach(function(el) {
                    deaths.push(el.TotalDeaths)
                })
                return deaths;
            }
            function getConirm(canvas) {
                var confirmed=[];

                canvas.Countries.forEach(function(el) {
                  confirmed.push(el.TotalConfirmed)
                })
                return confirmed;
            }


            var colors=[];
            function color_random(){
                var r = Math.floor(Math.random() * 255);
                var g = Math.floor(Math.random() * 255);
                var b = Math.floor(Math.random() * 255);
                return "rgb(" + r +","+ g +","+ b +")";
            }

            for(var i in canvas.Countries) {
                colors.push(color_random());
            }

            var myChart = new Chart(ctx,{
                type: 'bar',
                data: {
                    labels: getContries(canvas),
                    datasets : [{
                        label: '# TotalRecofered',
                        data: getHealth(canvas),
                        backgroundColor: colors,
                        borderColor: colors,
                        borderWidth: 1
                    }]
                },
                options:{
                  legend:{
                    display:false
                  }
                }
            })

            var mySecondChart = new Chart(cty,{
              type: 'bar',
              data: {
                labels: getContries(canvas),
                datasets : [{
                  label: '# Death',
                  data: getDeath(canvas),
                  borderWidht: 1,
                  backgroundColor: colors,
                }],
              },
              options: {
                legend: {
                  display: false
                }
              }
            })

            var myThidChart = new Chart(ctz,{
              type: 'bar',
              data: {
                labels: getContries(canvas),
                datasets: [{
                  label: '# Confirmed',
                  data: getConirm(canvas),
                  borderWidht: 1,
                  backgroundColor: colors,
                }],
              },
              options: {
                legend: {
                  display: false
                }
              }
            })
        });
    </script>
  </div>

  <footer class="bg-dark">
      <div class="container-fluid pt-5">
        <div class="row mt-2">
          <div class="container">
              <div class="row">
               <div class="col-lg-12">
                  <center><a style="text-decoration: none;" class="text-light mt-5" href="https://covid19.go.id/"><img src="images/logo satgas.png" width="80" height="80" alt=""><br> <br> SATGAS PENANGANAN COVID-19</a></center>
                </div>
                <div class="col-lg-12 align-self-center" style="text-align:center;">
                  <ul class="social-icon list-inline pt-2">
                    <li class="list-inline-item p-3">
                      <a href="https://www.facebook.com/InfoBencanaBNPB/"><i style="font-size: 2rem; color:wheat" class="fab fa-facebook-f"></i></a>
                    </li>
                    <li class="list-inline-item p-3">
                     <a href="https://www.instagram.com/bnpb_indonesia/"><i style="font-size: 2rem; color:wheat" class="fab fa-instagram"></i></a>
                  </li>
                  <li class="list-inline-item p-3">
                    <a href="https://twitter.com/bnpb_indonesia"><i style="font-size: 2rem; color:wheat" class="fab fa-twitter"></i></a>
                  </li>
                </ul>
              </div>
              <div class="col text-white">
                  <p class="mt-0" style="text-align: center;">&copy;CopyRight 12-11-2020</p>
                  <hr width="63%" class="border-light">
              </div>
            </div>
          </div>
        </div>
    </div>
  </footer>

 

    <!-- Optional JavaScript; choose one of the two! -->

    <!-- Option 1: jQuery and Bootstrap Bundle (includes Popper) -->
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>

  </body>
</html>